# Lab 1 - VM CentOS Base

## Required packages
 - VirtualBox
 - Vagrant
 - Git

1. Clone repository
```shell
$ git clone git@gitlab.com:beasd/inuits-bootcamp.git
```

2. Change directory to lab01
```shell
$ cd inuits-bootcamp/lab01
```

3. Create virtual machine in VirtualBox with Vagrant
```shell
$ vagrant up
```
4. Vagrant will automatically provision the virtual machine with the following command's:
```shell
$ yum install -y python3
$ useradd admin -p admin
$ echo "admin ALL=(ALL) ALL" >> /etc/sudoers
$ useradd admin -p $(openssl passwd -1 admin)
```
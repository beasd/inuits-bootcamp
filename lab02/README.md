# Lab 1 - VM CentOS Base

## Required packages
 - VirtualBox
 - Vagrant
 - Git

1. Clone repository
```shell
$ git clone git@gitlab.com:beasd/inuits-bootcamp.git
```

2. Change directory to lab02
```shell
$ cd inuits-bootcamp/lab02
```

3. Create virtual machine in VirtualBox with Vagrant
```shell
$ vagrant up
```
